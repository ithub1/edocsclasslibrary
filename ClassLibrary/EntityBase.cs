﻿using ClassLibrary.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ClassLibrary
{
    public class EntityBase
    {
        public EntityBase()
        {
            this.UUID = Guid.NewGuid();
            this.CreateDate = new DateTime().GetByCultureInfo();
            this.LastUpdate = new DateTime().GetByCultureInfo();
        }

        [JsonProperty("UUID")]
        public Guid UUID { get; set; }

        [JsonProperty("CreateDate")]
        public DateTime CreateDate { get; set; }

        [JsonProperty("LastUpdate")]
        public DateTime LastUpdate { get; set; }

        [JsonProperty("IsDeleted")]
        public bool IsDeleted { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }
    }
}
