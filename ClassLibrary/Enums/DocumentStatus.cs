﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary.Enums
{
    public enum DocumentStatus
    {
        Pending = 'P',
        Signed = 'S',
        Authorized = 'A',
        Completed = 'C',

    }
}
