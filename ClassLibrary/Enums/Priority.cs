﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary.Enums
{
    public enum Priority
    {
        Low='L',
        Medium='M',
        High='H',
    }
}
