﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary
{
    public class SignImage:EntityBase
    {
        [JsonProperty("Url")]
        public string Url { get; set; }
    }
}
