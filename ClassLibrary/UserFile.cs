﻿using ClassLibrary.Catalogs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary
{
    public class UserFile:EntityBase
    {
        [JsonProperty("UploaderUser")]
        public User UploaderUser { get; set; }

        [JsonProperty("DocumentFile")]
        public DocumentFile DocumentFile { get; set; }

        [JsonProperty("ActionUser")]
        public User ActionUser { get; set; }

        [JsonProperty("ActionRol")]
        public Rol ActionRol { get; set; }

        [JsonProperty("Signed")]
        public bool Signed { get; set; }
    }
}
