﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary.Catalogs
{
    public class Position:EntityBase
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
    }
}
