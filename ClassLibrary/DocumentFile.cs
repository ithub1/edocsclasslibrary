﻿using ClassLibrary.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary
{
    public class DocumentFile:EntityBase
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Url")]
        public string Url { get; set; }

        [JsonProperty("Priority")]
        public Priority Priority { get; set; }

        [JsonProperty("DocumentStatus")]
        public DocumentStatus DocumentStatus { get; set; }

        [JsonProperty("Comments")]
        public string Comments { get; set; }
    }
}
