﻿using ClassLibrary.Catalogs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary
{
    public class User:EntityBase
    {
        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Position")]
        public Position Position { get; set; }

        [JsonProperty("Rol")]
        public Rol Rol { get; set; }

        [JsonProperty("SignAlias")]
        public string SignAlias { get; set; }

        [JsonProperty("SignImage")]
        public SignImage SignImage { get; set; }

    }
}
